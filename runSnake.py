import sys
import os
import datetime
from tqdm import tqdm
import numpy as np
import threading as th
import queue

from Snake.Snake import SnakeGameAI, SnakeGameHumanPlayer
from AI.agent import initAgent
from Snake.Pygame import main as pygame
from config import *

# Set up a SnakeGame and let user play
def play():
    # queue for msgs from snake to rendering with info about state, game_over and score (not implemented)
    msg_snake_2_pygame = queue.Queue()
    # queue for msgs from rendering to snake with info about events. Can be used to trigger a cycle of snake to synchronise with rendering
    msg_pygame_2_snake = queue.Queue()

    # Init SnakeGameHumanPlayer class
    snake_game = SnakeGameHumanPlayer(queue_in=msg_pygame_2_snake, queue_out=msg_snake_2_pygame)

    # Init and start pygame thread
    th_pygame = th.Thread(target=pygame, name="Pygame", args=(msg_snake_2_pygame, msg_pygame_2_snake, snake_game.size,))
    th_pygame.start()

    # run snake
    snake_game.run()

    # wait until pygame thread terminates
    th_pygame.join()

# Start Training Cycle: Training -> Learning -> Evaluation
def training(project_dir, num_training_games=NUM_TRAINING_GAMES, num_learning_cylces=NUM_LEARNING_CYCLES, num_evaluations_games=NUM_EVALUATION_GAMES):
    timestamp = getTimestamp()

    # Init new agent or load if an agent save file exists in project directory
    agent = initAgent(project_dir)

    ######### TRAINING ##################################################################
    # turn on saving of experiences to buffer
    agent.training_mode = True

    print("\nStart Training Cycle:")
    while True:
        for i in tqdm(range(num_training_games)):
            # Init SnakeGameAI class with agent and without rendering
            snake_game = SnakeGameAI(agent=agent)
            snake_game.run()

        # train until defined number of saved experiences is reached
        if agent.buffer.num_experiences > NUM_EXPERIENCES_TO_START_LEARNING: break

    # print info about saved experiences
    print("Number of saved experiences: %d"%(agent.buffer.num_experiences))
    agent.Training_mode = False


    ######### LEARNING ##################################################################
    loss = accuracy = 0

    print("\nStart Learning Cycle:")
    for i in tqdm(range(num_learning_cylces)):
        a, l = agent.learn()
        accuracy += a
        loss += l
        # Todo activate target network update
        #agent.network.updateTarget()

    print("Loss: %s    Accuracy: %s"%(loss/num_learning_cylces, accuracy/num_learning_cylces))


    ######### EVALUATION ##################################################################
    # create empty list to store returned rewards while evaluating
    reward = np.zeros(num_evaluations_games, dtype=float)

    print("\nStart Evaluation Cycle:")
    for i in tqdm(range(num_evaluations_games)):
        # play a evaluation game with agent and save reward to list
        reward[i] = evaluation(agent)

    median_reward = np.median(reward)
    mean_reward = np.mean(reward)


    ###### POSTPROCESSING ######
    # save agent in project dir and append evaluation file
    saveProgress(agent, project_dir, timestamp, loss, accuracy, median_reward, mean_reward)

# Start Snake with given agent as Player. Returns the collected reward.
def evaluation(agent):
    # queue for msgs from snake to rendering with info about state, game_over and score (not implemented)
    msg_snake_2_pygame = queue.Queue()
    # queue for msgs from rendering to snake with info about events. Can be used to trigger a cycle of snake to synchronise with rendering
    msg_pygame_2_snake = queue.Queue()

    # Init SnakeGameAI class with given agent and with rendering
    snake_game = SnakeGameAI(agent=agent, queue_in=msg_pygame_2_snake, queue_out=msg_snake_2_pygame, render=True)

    th_pygame = th.Thread(target=pygame, name="Pygame", args=(msg_snake_2_pygame, msg_pygame_2_snake, snake_game.size,))
    th_pygame.start()

    reward = snake_game.run()

    th_pygame.join()

    return reward

def getTimestamp():
    dt = datetime.datetime.now()
    year = str(dt.year)[-2:]
    return "{:s}{:02d}{:02d}_{:02d}{:02d}{:02d}".format(year, dt.month, dt.day, dt.hour, dt.minute, dt.second)


def saveProgress(agent, project_dir, timestamp, loss, accuracy, median_reward, mean_reward):
    print("\nPostprocessing:")
    print(" - Write evaluation file")

    # append evaluation file with timestamp, completed_games, network loss, network accuracy median reward and mean reward
    with open(os.path.join(project_dir, EVALUATION_FILE_NAME), 'a') as f:
        f.write("%s %s %s %s %s %s\n" % (timestamp, agent.completed_games, loss, accuracy, median_reward, mean_reward))

    print(" - Save agent")

    if median_reward > agent.highscore:
        print(" - New highscore: agent saved to /%s" % (BEST_SAVEPOINT_DIR_NAME))
        agent.highscore = median_reward
        agent.save(os.path.join(project_dir, BEST_SAVEPOINT_DIR_NAME))

    # overwrite agent.sav in latest
    agent.save(os.path.join(project_dir, LAST_SAVEPOINT_DIR_NAME))


if __name__ == "__main__":
    mode = sys.argv[1]
    if len(sys.argv) > 2:
        save_path = os.path.join(".", "Data", sys.argv[2])

    if mode == "HumanPlayerMode":
        play()
    elif mode == "AIPlayerMode":
        agent = loadAgent(save_path)
        evaluation(agent)
    elif mode == "AITraining":
        training(save_path)