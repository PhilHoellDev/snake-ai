import os
import pickle
import random
import tensorflow as tf
import keras.losses
import numpy as np
from copy import copy

from config import *
from AI.buffer import PrioritizedReplayBuffer
from AI.network import NeuralNetwork
from AI.greedy import EpsilonGreedy

class Agent:
    def __init__(self, path):
        self.path = path
        self.buffer = PrioritizedReplayBuffer(DEFAULT_BUFFER_SIZE)
        self.network = NeuralNetwork(NETWORK_LAYERS, NETWORK_ACTIVATION, NETWORK_LOSS_FUNCTION, NETWORK_LEARNING_RATE, NETWORK_METRICS)
        # Todo Refactor EpsilonGreedy
        self.greedy = EpsilonGreedy()
        self.training_mode = False
        self.num_actions = NUM_ACTIONS
        self.batch_size = BATCH_SIZE
        self.gamma = DISCOUNTING_FACTOR

        self.completed_games = 0
        self.highscore = -100

    def getAction(self, state):
        if self.training_mode and self.greedy.useGreedy():
            return self.getRandomAction()

        action = self.getNetworkAction([state])[0]
        action = action.tolist()
        return action

    def getRandomAction(self):
        index = random.randint(0, self.num_actions - 1)
        action = [0] * self.num_actions
        action[index] = 1
        return action

    def getNetworkAction(self, state_list):
        batch_prediction = self.getPrediction(state_list)
        action_batch = []
        for prediction in batch_prediction:
            action_index = np.argmax(prediction)
            action = np.zeros(len(prediction), dtype=float)
            action[action_index] = 1
            action_batch.append(action)

        return action_batch

    def saveExperience(self, state: list, action: list, reward: list, new_state: list, done: list) -> None:
        if not self.training_mode: return
        self.buffer.add(state, action, reward, new_state, done)
        if done: self.completed_games += 1

    def learn(self):
        state, next_state, action, reward, done, index = self.getExperiencesFromMemory()

        q_values_state = self.getPrediction(state)
        q_values_next_state = self.getPrediction(next_state, model='target')
        q_values_target = copy(q_values_state)

        q_values_target[np.arange(self.batch_size), action] = reward + self.gamma * np.max(q_values_next_state) * (1 - done)

        mse = keras.losses.MeanSquaredError(reduction=tf.compat.v1.losses.Reduction.NONE)
        error = mse(q_values_state, q_values_target).numpy()
        self.buffer.update(index, error)

        state = [np.concatenate(s) for s in state]
        accuracy, loss = self.network.train(np.array(state), q_values_target)
        return [accuracy, loss]

    def getExperiencesFromMemory(self):
        return self.buffer.sample(self.batch_size)

    def getPrediction(self, state_list, model='evaluation'):
        state_list = [np.concatenate(state) for state in state_list]

        if model == 'target':
            return self.network.predict(np.array(state_list), use_target_network=True)
        return self.network.predict(np.array(state_list))

    def save(self, path):
        path_agent = os.path.join(path, "agent.sav")
        path_model = os.path.join(path, NETWORK_SAVEFILE_NAME)
        with open(path_agent, 'wb') as f:
            pickle.dump(self, f)
        self.network.save(path_model)

def initAgent(path):
    project = path.split(os.sep)[-1]
    agent_path = os.path.join(path, LAST_SAVEPOINT_DIR_NAME, AGENT_SAVEFILE_NAME)

    # Create new project directory or load existing
    if os.path.exists(agent_path):
        print("===== CONTINUE TRAINING: '%s'" %(project))
        print("Preprocessing:")
        print("  - Load agent")
        agent = loadAgent(agent_path)

    else:
        print("===== NEW TRAINING: '%s'"%(project))
        print("  - Create new project directory: '%s'" % (path))
        if not os.path.exists(path):
            os.mkdir(path)
            os.mkdir(os.path.join(path, LAST_SAVEPOINT_DIR_NAME))
            os.mkdir(os.path.join(path, BEST_SAVEPOINT_DIR_NAME))

        print("  - Initialize agent")
        agent = Agent(agent_path)

        print("  - Initialize evaluation.log")
        # create evaluation.log and write first line with colum names
        if not os.path.exists(os.path.join(path, EVALUATION_FILE_NAME)):
            with open(os.path.join(path, EVALUATION_FILE_NAME), 'w') as f:
                f.write("Timestamp   CompletedGames   Loss   Accuracy   MedianReward   MeanReward\n")

    return agent

def loadAgent(path_agent):
    path_network = os.path.split(path_agent)[:-1]
    path_network = os.path.join(*path_network, NETWORK_SAVEFILE_NAME)

    try:
        with open(path_agent, 'rb') as handle:
            agent = pickle.load(handle)
    except:
        raise Exception("Can't load agent from: '%s'"%(path_agent))

    try:
        agent.network.load(path_network)
    except:
        raise Exception("Can't load network from: '%s'"%(path_network))

    return agent

# for debugging
def printState(state, next):
    print("### STATE ###")
    print(" _ _ _ _ _ _ _ _ _ _            _ _ _ _ _ _ _ _ _ _")
    for row in range(10):
        print("|", end="")
        for col in range(10):
            if state[col, row] == 1:
                print("+", end=" ")
            elif state[col, row] == 2:
                print("O", end=" ")
            elif state[col, row] == 3:
                print("#", end=" ")
            else: print(" ", end=" ")
        print("|", end="        |")

        for col in range(10):
            if next[col, row] == 1:
                print("+", end=" ")
            elif next[col, row] == 2:
                print("O", end=" ")
            elif next[col, row] == 3:
                print("#", end=" ")
            else:
                print(" ", end=" ")
        print("|", end="\n")
    print(" ````````````````````          ````````````````````")

def printPrediction(pre, prediction):
    print(pre, end="")
    rechts = prediction[2]
    links = prediction[3]
    hoch = prediction[0]
    runter = prediction[1]
    print("  Rechts: %s   Links: %s   Hoch: %s   Runter: %s"%(rechts, links, hoch, runter))

def getActionName(action):
    if action == 0: return "Hoch"
    if action == 1: return "Runter"
    if action == 2: return "Rechts"
    if action == 3: return "Links"




