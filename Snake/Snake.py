import sys
import random
import numpy as np
import queue
from copy import copy
from .config import *

class SnakeGame:
    def __init__(self, queue_in=None, queue_out=None, size=DEFAULT_GAME_GRID_SIZE):
        self.queue_out = queue_out
        self.queue_in = queue_in
        self.size = size

        self.score = 0
        self.found_food = False
        self.game_over = False

        if size[0] < MIN_FIELD_SIZE[0] or size[1] < MIN_FIELD_SIZE[1]:
            raise Exception("Playing field size must be at least 5 x 3.")

        # create initial state
        snake_vertical_position = int(self.size[1] // 2)
        self.state = self.getInitialState(snake_vertical_position)
        self.snake_position = self.getSnakePosition(snake_vertical_position)

        self.next_move = np.array(MOVE_RIGHT)
        self.placeNewFood()

    def getInitialState(self, snake_vertical_position):
        initial_state = np.zeros(self.size, dtype=int)

        for col in initial_state[1:SNAKE_INIT_LENGTH+1]:
            col[snake_vertical_position] += SNAKE_BODY_VALUE

        col[snake_vertical_position] += SNAKE_HEAD_VALUE - SNAKE_BODY_VALUE

        return initial_state

    def getSnakePosition(self, snake_vertical_position):
        return [np.array([SNAKE_INIT_LENGTH - i, snake_vertical_position]) for i in range(SNAKE_INIT_LENGTH)]

    ### Game Events ###

    def handleGameEvents(self):
        if self.outOfBounds() or self.bitesItself():
            self.game_over = True
            return

        if self.foundFood():
            self.score += 10
            self.placeNewFood()
            self.found_food = True

    def outOfBounds(self):
        new_head_position = self.snake_position[0] + self.next_move

        if new_head_position[0] < 0 or new_head_position[0] >= self.size[0]:
            return True
        if new_head_position[1] < 0 or new_head_position[1] >= self.size[1]:
            return True

        return False

    def bitesItself(self):
        new_head_position = self.snake_position[0] + self.next_move
        if self.state[new_head_position[0]][new_head_position[1]] == SNAKE_BODY_VALUE:
            return True
        return False

    def foundFood(self):
        new_head_position = self.snake_position[0] + self.next_move
        if self.state[new_head_position[0]][new_head_position[1]] == FOOD_VALUE:
            return True
        return False

    def placeNewFood(self):
        x = random.randint(0, self.size[0] - 1)
        y = random.randint(0, self.size[1] - 1)

        if self.state[x][y] != 0:
            self.placeNewFood()
            return

        self.state[x][y] = FOOD_VALUE

    ### Movement ###

    def move(self):
        if not self.found_food: self.moveTail()
        self.moveHead()

    def moveHead(self):
        head_position = self.snake_position[0]
        new_head_position = head_position + self.next_move

        self.state[head_position[0]][head_position[1]] = SNAKE_BODY_VALUE
        self.state[new_head_position[0]][new_head_position[1]] += SNAKE_HEAD_VALUE

        if self.found_food:
            self.state[new_head_position[0]][new_head_position[1]] -= FOOD_VALUE

        self.snake_position.insert(0, new_head_position)

    def moveTail(self):
        tail_position = self.snake_position[-1]

        self.state[tail_position[0]][tail_position[1]] -= SNAKE_BODY_VALUE

        self.snake_position.pop(-1)

    ### Misc ###

    def getPygameMsg(self, pygame_msg=None):
        try:
            return self.queue_in.get(timeout=2)
        except queue.Empty:
            print("Timeout while waiting for Pygame Events")
            return pygame_msg

    def sendPygameMsg(self):
        msg = {
            SNAKE_MSG_STATE__NAME: np.ndarray.tolist(self.state),
            SNAKE_MSG_GAMEOVER__NAME: self.game_over
        }

        self.queue_out.put(msg)

    def printState(self):
        for row in range(len(self.state[0])):
            for col in self.state:
                print(col[row], end= " ")
            print()
        print()


class SnakeGameAI(SnakeGame):
    def __init__(self, agent, queue_in=None, queue_out=None, size=DEFAULT_GAME_GRID_SIZE, render=False):
        super().__init__(queue_in=queue_in, queue_out=queue_out, size=size)
        self.agent = agent
        self.render = render;

        # No rendering possible if one of both queues is missing
        if self.render and (not queue_in or not queue_out): raise Exception("Can`t display game without queue.")

    def run(self):
        reward_game = 0
        steps_without_food = 0
        run = True

        while run:
            # sync with pygame by waiting for the msg pygame sends in every cycle
            if self.render:
                self.getPygameMsg()

            # get action from network
            action = self.getAction(self.state)

            # set direction of next move
            self.setNextMove(action)

            # Handle Game Events like head will be out of bounds, biting a body element or has found food.
            self.handleGameEvents()

            # calculate reward
            reward = REWARD_STEP
            if self.found_food: reward += REWARD_FOOD
            if self.game_over: reward += REWARD_GAME_OVER

            # send state and quit msg to pygame, save experience and return
            if self.game_over or steps_without_food > 100:
                self.saveExperience(self.state, action, reward, self.state, self.game_over)
                self.game_over = True
                if self.render: self.sendPygameMsg()
                reward_game += reward
                if steps_without_food > 100: print("Force Quit")
                return reward_game

            # copy old state before moving
            old_state = copy(self.state)

            # perform the previously set move
            self.move()

            # send game info to pygame for rendering
            if self.render:
                self.sendPygameMsg()

            # save experience
            self.saveExperience(old_state, action, reward, copy(self.state), self.game_over)

            reward_game += reward
            if self.found_food:
                self.found_food = False
                steps_without_food = 0
            else:
                steps_without_food += 1


    def getAction(self, state):
        action = self.agent.getAction(state)
        return action

    def setNextMove(self, action):
        previous_move = copy(self.next_move)

        possible_moves = [MOVE_UP, MOVE_DOWN, MOVE_RIGHT, MOVE_LEFT]
        next_move = possible_moves[action.index(1)]

        elementwise_sum = next_move + previous_move

        #if not np.array_equal(elementwise_sum, [0, 0]):
        self.next_move = np.array(next_move)

    def saveExperience(self, state, action, reward, new_state, done):
        self.agent.saveExperience(state, new_state, action, reward, done)


class SnakeGameHumanPlayer(SnakeGame):
    def __init__(self, queue_in, queue_out, size=DEFAULT_GAME_GRID_SIZE):
        super().__init__(queue_in=queue_in, queue_out=queue_out, size=size)
        self.render = True

    def run(self):
        pygame_msg = None
        run = True

        while run:
            # get info from Pygame if available.
            pygame_msg = self.getPygameMsg(pygame_msg)

            # set direction of next move based on the pressed key and the current movement
            self.setNextMove(pygame_msg[PYGAME_MSG_KEYS__NAME])

            # Handle Game Events like head will be out of bounds, biting a body element or has found food.
            self.handleGameEvents()
            if self.game_over:
                self.sendPygameMsg()
                break

            # perform the previously set move
            self.move()

            # send game info to pygame for rendering
            self.sendPygameMsg()

            self.found_food = False
            run = not pygame_msg[PYGAME_MSG_QUIT__NAME]

    def setNextMove(self, key):
        next_move = copy(self.next_move)
        previous_move = copy(self.next_move)

        if key == PYGAME_MSG_KEYS_UP__NAME: next_move = MOVE_UP
        elif key == PYGAME_MSG_KEYS_DOWN__NAME: next_move = MOVE_DOWN
        elif key == PYGAME_MSG_KEYS_LEFT__NAME: next_move = MOVE_LEFT
        elif key == PYGAME_MSG_KEYS_RIGHT__NAME: next_move = MOVE_RIGHT

        elementwise_sum = next_move + previous_move

        if not np.array_equal(elementwise_sum, [0, 0]):
            self.next_move = np.array(next_move)