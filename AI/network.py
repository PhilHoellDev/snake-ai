import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import keras
from keras.models import Sequential
from keras.layers import Dense, Input
from keras.callbacks import History
#history = History()
import tensorflow as tf
from config import *

class NeuralNetwork:
    def __init__(self, layers, activation, loss, learning_rate, metrics):
        optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
        self.evaluation = buildModel(layers, activation, loss, optimizer, metrics)
        self.target = buildModel(layers, activation, loss, optimizer, metrics)

    def predict(self, input, use_target_network=False):
        if use_target_network:
            return self.target.predict(input)
        return self.evaluation.predict(input)

    def train(self, state, target):
        history = self.evaluation.fit(state, target, verbose=0)
        return history.history['loss'][0], history.history['accuracy'][0]

    def save(self, path):
        self.evaluation.save(path)

    def load(self, path):
        self.evaluation = keras.models.load_model(path)
        self.target = keras.models.load_model(path)

    def updateTarget(self):
        self.target.set_weights(self.evaluation.get_weights())

def buildModel(layers, activation, loss_function, optimizer, metrics):
    model = Sequential()
    model.add(Input(shape=(layers[0], )))

    for i in range(1, len(layers)-1):
        model.add(Dense(layers[i], activation=activation[i]))

    model.add(Dense(layers[-1], activation=activation[-1]))

    model.compile(loss=loss_function, optimizer=optimizer, metrics=['accuracy'])

    return model

