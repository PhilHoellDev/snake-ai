from .config import *
import random

class EpsilonGreedy:
    def __init__(self):
        self.epsilon = EPSILON_INITIAL
        self.epsilon_end = EPSILON_MINIMAL
        self.epsilon_fac = EPSILON_DECREASING_FACTOR

    def useGreedy(self):
        if random.random() <= self.epsilon:
            return True
        return False

    def decrease(self):
        pass
        #decrease epsilon

    def save(self):
        #save epsilon greedy state
        pass

    def load(self):
        #load epsilon greedy state
        pass