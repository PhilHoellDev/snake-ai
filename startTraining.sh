#!/bin/bash

project=test
iterations=1

source ./venv/bin/activate

for (( i = 0; i < $iterations; i++ ))
do
    python3 runSnake.py AITraining $project
done