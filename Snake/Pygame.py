import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame
import time
from .config import *

#### Game Objects ####
class SnakeGame:
    def __init__(self, state_size):
        self.state = None
        self.game_over = False

        # Space that one value of state occupies on the screen
        self.grid_size = min(WINDOW_SIZE[0] // state_size[0], WINDOW_SIZE[1] // state_size[1])
        # Surface
        self.game_surface = GameSurface(state_size, self.grid_size)

        self.body_element = BodyElement((self.grid_size, self.grid_size))
        self.food = Food((self.grid_size, self.grid_size))

    def render(self, screen):
        # don`t render if state=None
        if not self.state: return

        # fill Game Surface with black
        self.renderGameSurface(screen)

        # go through every element of the state matrix
        self.renderState()

        # blit game_surface onto screen
        screen.blit(self.game_surface.surface, self.game_surface.pos)

    def renderGameSurface(self, screen):
        self.game_surface.surface.fill(BLACK)

    def renderState(self):
        for x, col in enumerate(self.state):
            for y, value in enumerate(col):
                if value == SNAKE_BODY_VALUE or value == SNAKE_HEAD_VALUE:
                    pos = self.getScreenPos(x, y)
                    self.renderBody(self.game_surface.surface, pos)
                elif value == FOOD_VALUE:
                    # draw food onto game_surface
                    pos = self.getScreenPos(x, y)
                    self.renderFood(self.game_surface.surface, pos)

    def renderBody(self, screen, pos):
        screen.blit(self.body_element.surface, pos)

    def renderFood(self, screen, pos):
        screen.blit(self.food.surface, pos)

    def displayGameOver(self, screen):
        font = pygame.font.SysFont(None, GAMEOVER_FONT_SIZE)
        game_over_text = font.render(SNAKE_MSG_GAMEOVER__NAME, True, RED)
        game_over_text_rect = game_over_text.get_rect(center=(WINDOW_SIZE[0] / 2, WINDOW_SIZE[1] / 2))
        screen.blit(game_over_text, game_over_text_rect)
        pygame.display.update()
        time.sleep(GAMEOVER_TIMEOUT)

    # get pos on screen [pixel] from index in state matrix
    def getScreenPos(self, x, y):
        x_ = self.grid_size * x
        y_ = self.grid_size * y
        return (x_, y_)

    # receive state msg and update corresponding values
    def updateGameState(self, state_queue):
        if state_queue.empty(): return

        # loop through all msgs received, so that all msgs get removed. Only the last msg will be used.
        while not state_queue.empty():
            msg = state_queue.get_nowait()

        self.state = msg[SNAKE_MSG_STATE__NAME]
        self.game_over = msg[SNAKE_MSG_GAMEOVER__NAME]

class BodyElement:
    def __init__(self, size):
        self.size = size
        self.surface = self.createSurface()

    def createSurface(self):
        surface = pygame.Surface(self.size)

        # draw a white rect within a black rect
        outer_rect = pygame.Rect((0, 0), self.size)
        inflation = int(self.size[0] * RECT_INFLATION / 2) * 2
        inner_rect = outer_rect.inflate(-inflation, -inflation)

        pygame.draw.rect(surface, BLACK, outer_rect)
        pygame.draw.rect(surface, WHITE, inner_rect)

        return surface

class Food:
    def __init__(self, size):
        self.size = size
        self.surface = self.createSurface()

    def createSurface(self):
        surface = pygame.Surface(self.size)

        # draw a red rect within a black rect
        outer_rect = pygame.Rect((0, 0), self.size)
        inflation = int(self.size[0] * RECT_INFLATION / 2) * 2
        inner_rect = outer_rect.inflate(-inflation, -inflation)

        pygame.draw.rect(surface, BLACK, outer_rect)
        pygame.draw.rect(surface, RED, inner_rect)

        return surface

class GameSurface:
    def __init__(self, state_size, grid_size):
        self.size = (grid_size * state_size[0], grid_size * state_size[1])
        self.pos = ((WINDOW_SIZE[0] - self.size[0]) // 2, (WINDOW_SIZE[1] - self.size[1]) // 2)
        self.surface = pygame.Surface(self.size)
        self.surface.fill(BLACK)

#### MAIN ####
def main(state_queue, pygame_queue, state_size):
    pygame.init()
    screen = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption(TITLE)
    clock = pygame.time.Clock()
    game = SnakeGame(state_size)

    run(game, screen, clock, state_queue, pygame_queue)
    pygame.quit()

def run(game, screen, clock, state_queue, pygame_queue):
    run = True
    while run:
        clock.tick(FPS)

        # get events from pygame and send them out
        events = getEvents()
        keys_pressed = getKeysPressed()
        sendEvents(pygame_queue, events, keys_pressed)

        # receive incoming state msg and update game
        game.updateGameState(state_queue)

        # render game
        screen.fill(GREY)
        game.render(screen)
        pygame.display.update()

        if game.game_over:
            game.displayGameOver(screen)
            return

        run = not events[PYGAME_MSG_QUIT__NAME]

def getEvents():
    events = {
        PYGAME_MSG_QUIT__NAME: False
    }

    pygame_events = pygame.event.get()
    for event in pygame_events:
        if event.type == pygame.QUIT:
            events[PYGAME_MSG_QUIT__NAME] = True

    return events

def getKeysPressed():
    keys_pressed = None
    pygame_keys = pygame.key.get_pressed()

    if pygame_keys[pygame.K_UP]: keys_pressed = PYGAME_MSG_KEYS_UP__NAME
    elif pygame_keys[pygame.K_DOWN]: keys_pressed = PYGAME_MSG_KEYS_DOWN__NAME
    elif pygame_keys[pygame.K_LEFT]: keys_pressed = PYGAME_MSG_KEYS_LEFT__NAME
    elif pygame_keys[pygame.K_RIGHT]: keys_pressed = PYGAME_MSG_KEYS_RIGHT__NAME

    return keys_pressed

def sendEvents(queue, events, keys):
    msg = {
        PYGAME_MSG_KEYS__NAME: keys
    }
    for entry in events:
        msg[entry] = events[entry]

    queue.put(msg)

if __name__ == "__main__":
    main()