import random
import numpy as np
import pickle

# Error Value for new experiences (error not calculated yet)
DEFAULT_ERROR = 1

# Default filename of buffer save file
BUFFER_DEFAULT_FILENAME = 'buffer.save'

#Todo Add done flag
#Todo change add attribute order

### REPLAY BUFFER ###
# Can store a certain number of experiences, defined by 'size'. If the buffer is full, a new experience will
# overwrite the oldest one. Experiences can be sampled batchwise or individually. The sampling is done with a
# probability distribution that is proportional to the error of the experience.
class PrioritizedReplayBuffer:
    def __init__(self, size):
        self.size = size                                    # num of experiences which can be stored within the buffer
        self.tree = SumTree(size)                           # sum tree for prioritized sampling
        self.data = np.zeros(size, dtype=Experience)        # list with stored experiences
        self.num_experiences = 0                            # total number of seen experiences.
        self.index = 0


    # add a experience to the buffer
    def add(self, state, next_state, action, reward, done):
        # create a Experience object with the given args
        action = action.index(1)
        experience = Experience(state, next_state, action, reward, done)

        # save the experience into 'self.data'. If buffer is full, start again at index 0.
        # -> Oldest experiences will be overwritten
        insert_index = self.num_experiences % self.size
        self.data[insert_index] = experience

        # add the error of the experience to the sumTree
        self.tree.add(insert_index, experience.error)

        # increase number of experiences seen
        self.num_experiences += 1

    # sample one experience or a batch of experiences
    def sample(self, batch_size=1):
        # create empty lists
        state = np.zeros(batch_size, dtype=list)
        next_state = np.zeros(batch_size, dtype=list)
        action = np.zeros(batch_size, dtype=int)
        reward = np.zeros(batch_size, dtype=float)
        done = np.zeros(batch_size, dtype=bool)

        # get indizes of priority sampled experiences
        data_index = self.tree.sample(batch_size)

        # fill lists with the corresponding values of the sampled experiences
        for i, index in enumerate(data_index):
            state[i] = np.array(self.data[index].state)
            next_state[i] = np.array(self.data[index].next_state)
            action[i] = self.data[index].action
            reward[i] = self.data[index].reward
            done[i] = self.data[index].done

        # Return the first element of each list, if they only contain one value
        if batch_size == 1:
            return state[0], next_state[0], action[0], reward[0], done[0], data_index[0]
        # Else return the lists
        return state, next_state, action, reward, done, data_index

    # update the error value of a specific experience
    def update(self, index, error):
        for i in range(len(index)):
            # update error value of experience
            self.data[index[i]].error = error[i]
            # update error value for prioritization
            self.tree.add(index[i], error[i])

    # save ReplayBuffer object
    def save(self, filename=BUFFER_DEFAULT_FILENAME):
        with open(filename, 'wb') as file:
            pickle.dump(self, file)

    # load ReplayBuffer object
    def load(filename=BUFFER_DEFAULT_FILENAME):
        with open(filename, 'rb') as file:
            buffer = pickle.load(file)
            return buffer

### SUM TREE ###
# Represents a binary search tree (but is stored as a list). A parent node is the sum of its two children.
# The errors of the experiences are stored on the lowest level of the tree. The position of the experience
# within the lowest level corresponds to its index in the buffer.
class SumTree:
    def __init__(self, capacity):
        self.capacity = capacity                                # number of elements on the lowest level
        self.tree = np.zeros(2 * self.capacity - 1)             # list representation of the tree

    # add an error value to the lowest level of the tree and propagate the change through the nodes
    def add(self, buffer_index, error):
        # get tree index from buffer index
        tree_index = self.getTreeIndex(buffer_index)

        # calculate difference of new and old error
        change = error - self.tree[tree_index]

        # overwrite the old value with the new error
        self.tree[tree_index] = error

        # get tree index of parent node
        parent_index = self.getParent(tree_index)

        # update the sum of every relevant node
        self.propagateChange(parent_index, change)

    # propagating the change to the top level
    def propagateChange(self, tree_index, change):
        # add change to current node
        self.tree[tree_index] += change

        # return if top level is reached
        if tree_index == 0: return

        # Call the function recursively with parent node
        parent_index = self.getParent(tree_index)
        self.propagateChange(parent_index, change)

    # return a random batch of indices or a random single index.
    # the selection probability is proportional to the respective error
    def sample(self, batch_size=1):
        #empty list
        sample_index = np.zeros(batch_size, dtype=int)

        # fill list with sampled indizes
        for i in range(batch_size):
            # random value from 0 to the sum of all errors stored
            value = random.random() * self.getTotal()
            # sample and store index
            sample_index[i] = self.getSampleIndex(value, 0)

        return sample_index

    # return the index of the
    def getSampleIndex(self, value, tree_index):
        # get left and right child of current node
        left_child = self.getLeftChild(tree_index)
        right_child = self.getRightChild(tree_index)

        # return index if lowes level is reached
        if tree_index >= self.capacity - 1:
            # get buffer index from tree index
            return self.getBufferIndex(tree_index)

        # if value is smaller then value of left child: take left child
        if value < self.tree[left_child]:
            child = left_child
        # else take right child and reduce value by value of left child
        else:
            value -= self.tree[left_child]
            child = right_child

        # Call the function recursively with child node
        return self.getSampleIndex(value, child)

    # get tree index from buffer_index
    def getTreeIndex(self, buffer_index):
        tree_index = buffer_index + self.capacity - 1
        return tree_index

    # get buffer index from tree index
    def getBufferIndex(self, tree_index):
        buffer_index = tree_index - self.capacity + 1
        return buffer_index

    # get parent node index from given index
    def getParent(self, tree_index):
        parent_index = tree_index / 2
        parent_index = np.ceil(parent_index) - 1
        return int(parent_index)

    # get left child index from given index
    def getLeftChild(self, tree_index):
        child_index = tree_index * 2 + 1
        return child_index

    # get right child index from given index
    def getRightChild(self, tree_index):
        child_index = tree_index * 2 + 2
        return child_index

    # get value of top node
    def getTotal(self):
        return self.tree[0]

    # print tree level by level (only for debugging)
    def print(self):
        num_levels = int(np.log2(self.capacity) + 1)
        level_start_index = 0
        level_end_index = 1
        for level in range(num_levels):
            nodes = self.tree[level_start_index:level_end_index]
            print(nodes)
            level_start_index = level_end_index
            level_end_index += pow(2, level + 1)

### Experience ###
# class for a single experience
class Experience:
    def __init__(self, state, next_state, action, reward, done):
        self.error = DEFAULT_ERROR
        self.state = state
        self.next_state = next_state
        self.action = action
        self.reward = reward
        self.done = done
